<?php

declare(strict_types=1);

namespace LightSource\AcfGroups\Interfaces;

use Exception;

interface CreatorInterface
{
    /**
     * @template T of AcfGroupInterface
     *
     * @param class-string<T> $groupClass
     *
     * @return T
     * @throws Exception
     */
    public function create(string $groupClass): AcfGroupInterface;
}
