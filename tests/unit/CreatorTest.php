<?php

declare( strict_types=1 );

use Codeception\Test\Unit;
use LightSource\AcfGroups\Creator;
use LightSource\AcfGroups\Interfaces\AcfGroupInterface;

class CreatorTest extends Unit {
	public function testCreate() {
		$group   = $this->makeEmpty( AcfGroupInterface::class );
		$creator = new Creator();

		$instance = $creator->create( get_class( $group ) );

		$this->assertTrue( $instance instanceof AcfGroupInterface );
	}

	public function testCreateCleansCreationChainAfterACall() {
		$group   = $this->makeEmpty( AcfGroupInterface::class );
		$creator = new Creator();

		$instance = $creator->create( get_class( $group ) );
		// if creationChain isn't clear then this second call should throw exception
		$secondInstance = $creator->create( get_class( $group ) );

		$this->assertTrue( $instance instanceof AcfGroupInterface );
		$this->assertTrue( $secondInstance instanceof AcfGroupInterface );
	}

	public function testCreateThrowsExceptionWhenClassIsMissing() {
		$creator = new Creator();

		$isWithException = false;
		try {
			$creator->create( 'some' );
		} catch ( Exception $exception ) {
			$isWithException = true;
		}

		$this->assertTrue( $isWithException );
	}

	public function testCreateThrowsExceptionWhenClassNotImplementsGroupInterface() {
		$group   = new class {
		};
		$creator = new Creator();

		$isWithException = false;
		try {
			$creator->create( get_class( $group ) );
		} catch ( Exception $exception ) {
			$isWithException = true;
		}

		$this->assertTrue( $isWithException );
	}

	public function testCreateThrowsExceptionWhenPermanentRecursion() {
		$creator = new Creator();
		global $c;

		$group = new class {
			public function __construct() {
				global $c;
				if ( ! ! $c ) {
					$c->create( new self() );
				}
			}
		};
		$c     = $creator;

		$isWithException = false;
		try {
			$creator->create( get_class( $group ) );
		} catch ( Exception $exception ) {
			$isWithException = true;
		}

		$this->assertTrue( $isWithException );
	}
}