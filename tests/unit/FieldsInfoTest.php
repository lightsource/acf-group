<?php

declare(strict_types=1);

use Codeception\Test\Unit;
use LightSource\AcfGroups\FieldInfo;
use LightSource\AcfGroups\FieldsInfo;

class FieldsInfoTest extends Unit
{
    public function testGetFieldsInfoWhenPublicVisibility(): void
    {
        $class = new class extends FieldsInfo {
            public string $fieldName;
        };

        $fieldsInfo = $class->getFieldsInfo();

        $this->assertCount(1, $fieldsInfo);
        $this->assertEquals('fieldName', $fieldsInfo[0]->getName());
    }

    public function testGetFieldsInfoWhenProtectedVisibilityIsIgnored(): void
    {
        $class = new class extends FieldsInfo {
            protected string $fieldName;
        };

        $this->assertEmpty($class->getFieldsInfo());
    }

    public function testGetFieldsInfoWhenUnsupportedTypeIsIgnored(): void
    {
        $class = new class extends FieldsInfo {
            protected self $fieldName;
        };

        $this->assertEmpty($class->getFieldsInfo());
    }

    public function testGetFieldsInfoWhenStaticIsIgnored(): void
    {
        $class = new class extends FieldsInfo {
            public static string $fieldName;
        };

        $this->assertEmpty($class->getFieldsInfo());
    }

    public function testGetFieldsInfoWhenOrderIsDefault(): void
    {
        $class = new class extends FieldsInfo {
            public string $firstField;
            public string $secondField;
        };

        $fieldsInfo = $class->getFieldsInfo();

        $this->assertCount(2, $fieldsInfo);
        $this->assertEquals('firstField', $fieldsInfo[0]->getName());
        $this->assertEquals('secondField', $fieldsInfo[1]->getName());
    }

    public function testGetFieldsInfoWhenOrderIsChanged(): void
    {
        $class = new class extends FieldsInfo {
            /**
             * @a-order 2
             */
            public string $secondField;
            /**
             * @a-order 1
             */
            public string $firstField;
        };

        $fieldsInfo = $class->getFieldsInfo();

        $this->assertCount(2, $fieldsInfo);
        $this->assertEquals('firstField', $fieldsInfo[0]->getName());
        $this->assertEquals('secondField', $fieldsInfo[1]->getName());
    }
}
