<?php

namespace Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;

class Unit extends \Codeception\Module
{
    public function getUniqueDirectory(string $methodConstant): vfsStreamDirectory
    {
        $dirName = str_replace([':', '\\'], '_', $methodConstant);

        return vfsStream::setup($dirName);
    }

    public function getUniqueNamespaceWithAutoloader(
        string $methodConstant,
        string $rootDirectory
    ): string {
        $namespace = str_replace('::', '_', $methodConstant);

        spl_autoload_register(
            function ($class) use ($rootDirectory, $namespace) {
                $targetNamespace = $namespace . '\\';
                if (0 !== strpos($class, $targetNamespace)) {
                    return;
                }

                $relativePathToFile = str_replace($targetNamespace, '', $class);
                $relativePathToFile = str_replace('\\', '/', $relativePathToFile);

                $absPathToFile = $rootDirectory . DIRECTORY_SEPARATOR . $relativePathToFile . '.php';

                include_once $absPathToFile;
            }
        );

        return $namespace;
    }

    public function getGroupClassFile(
        string $namespace,
        string $class,
        string $parentClass,
        string $classContent = '',
        bool $isAbstract = false
    ): string {
        $classDeclaration = ! $isAbstract ?
            'class' :
            'abstract class';

        return '<?php namespace ' . $namespace . '; ' . $classDeclaration . ' ' . $class . ' extends ' . $parentClass . ' {' . $classContent . '}';
    }
}
